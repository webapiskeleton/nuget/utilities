﻿namespace WebApiSkeleton.DatabaseMapping.Example;

public sealed record User
{
    public required int Id { get; init; }
    public required string FirstName { get; init; }
    public required string LastName { get; init; }
    public required int Age { get; init; }
}