﻿# Example

The example uses PostgreSQL database to show database interaction, but any other database provider is available for
usage. To execute the code the PostgreSQL instance is required. To connect to the specified instance connection string
can always be changed.