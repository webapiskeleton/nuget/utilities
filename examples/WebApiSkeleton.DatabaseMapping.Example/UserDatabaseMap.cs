namespace WebApiSkeleton.DatabaseMapping.Example;

public sealed class UserDatabaseMap : BaseDatabaseMapper<User>
{
    public UserDatabaseMap() : base("public.user")
    {
        MapProperty(x => x.Id, "id");
        MapProperty(x => x.FirstName, "first_name");
        MapProperty(x => x.LastName, "last_name");
        MapProperty(x => x.Age, "age");
    }
}