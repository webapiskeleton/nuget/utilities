using System.Reflection;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using WebApiSkeleton.Contracts.Base.ContractFeatureInterfaces;
using WebApiSkeleton.Contracts.Validation;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddMediatR(x => { x.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly()); });
builder.Services.AddContractValidatorsFromAssembly(Assembly.GetExecutingAssembly());

var validationBehavior = ValidationBehaviorCreator.GetValidationPipelineBehaviorForRequest(typeof(ExampleRequest));
// some behaviors added that will be executed before validation
builder.Services.AddTransient(validationBehavior.pipelineBehavior, validationBehavior.validationBehavior);
// other behaviors added that will be executed after validation

var app = builder.Build();


app.MapGet("/", async ([FromServices] ISender sender) =>
{
    var request = new ExampleRequest(0);
    var res = await sender.Send(request);
    return res.Match(Results.Ok, Results.BadRequest);
});

app.Run();

public sealed record ExampleRequest(int ValueMustBeGraterThanZero) : IValidatableRequest<int>;

public sealed class ExampleRequestValidator : AbstractValidator<ExampleRequest>
{
    public ExampleRequestValidator()
    {
        RuleFor(x => x.ValueMustBeGraterThanZero)
            .GreaterThan(0)
            .WithMessage("Must be grater than 0");
    }
}