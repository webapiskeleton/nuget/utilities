﻿using System.Reflection;
using FluentValidation;
using LanguageExt.Common;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using WebApiSkeleton.Contracts.Base.ContractFeatureInterfaces;

namespace WebApiSkeleton.Contracts.Validation;

public static class ServiceCollectionExtensions
{
    /// <summary>
    /// Add contract validators in specified assebmly
    /// </summary>
    public static IServiceCollection AddContractValidatorsFromAssembly(this IServiceCollection services,
        Assembly assembly, ServiceLifetime lifetime = ServiceLifetime.Scoped,
        Func<AssemblyScanner.AssemblyScanResult, bool> filter = null!,
        bool includeInternalTypes = false)
    {
        services.AddValidatorsFromAssembly(assembly, lifetime, filter, includeInternalTypes);
        return services;
    }

    /// <summary>
    /// Add contract validators in specified assemblies
    /// </summary>
    public static IServiceCollection AddContractValidatorsFromAssemblies(this IServiceCollection services,
        IEnumerable<Assembly> assemblies, ServiceLifetime lifetime = ServiceLifetime.Scoped,
        Func<AssemblyScanner.AssemblyScanResult, bool> filter = null!,
        bool includeInternalTypes = false)
    {
        services.AddValidatorsFromAssemblies(assemblies, lifetime, filter, includeInternalTypes);
        return services;
    }

    /// <summary>
    /// Add <see cref="ValidationBehavior{TRequest,TResponse}"/> for requests in specified assemblies
    /// </summary>
    public static IServiceCollection AddValidationPipelineBehaviorsFromAssemblies(this IServiceCollection services,
        IEnumerable<Assembly> assemblies)
    {
        foreach (var assembly in assemblies)
        {
            AddValidationPipelineBehaviorsFromAssembly(services, assembly);
        }

        return services;
    }

    /// <summary>
    /// Add <see cref="ValidationBehavior{TRequest,TResponse}"/> for requests in specified assembly
    /// </summary>
    public static IServiceCollection AddValidationPipelineBehaviorsFromAssembly(this IServiceCollection services,
        Assembly assembly)
    {
        var validators = assembly
            .GetTypes()
            .Where(x => x.BaseType is not null && x.BaseType.IsGenericType &&
                        x.BaseType.GetGenericTypeDefinition() ==
                        typeof(AbstractValidator<>).GetGenericTypeDefinition());
        foreach (var validator in validators)
        {
            var requestType = validator.BaseType!.GetGenericArguments().Single();
            if (!requestType.IsAssignableTo(typeof(IValidatableRequest)))
                continue;

            var responseType = requestType
                .GetInterface(typeof(IValidatableRequest<>).Name)?
                .GetGenericArguments()
                .Single();
            if (responseType is null)
                throw new InvalidOperationException(
                    $"Request that implements {nameof(IValidatableRequest)} must have a Result<T> return type: {requestType.Name}");
            var responseResultType = typeof(Result<>).MakeGenericType(responseType);

            services.AddTransient(typeof(IPipelineBehavior<,>).MakeGenericType(requestType, responseResultType),
                typeof(ValidationBehavior<,>).MakeGenericType(requestType, responseType));
        }

        return services;
    }
}