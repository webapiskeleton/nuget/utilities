﻿using FluentValidation;
using LanguageExt.Common;
using MediatR;
using WebApiSkeleton.Contracts.Base.ContractFeatureInterfaces;

namespace WebApiSkeleton.Contracts.Validation;

/// <summary>
/// Pipeline behavior that handled requests that implement <see cref="IValidatableRequest{TResponse}"/>
/// </summary>
/// <typeparam name="TRequest">Request type</typeparam>
/// <typeparam name="TResponse">Successful response</typeparam>
/// <returns><see cref="TResponse"/> if validation is successful, otherwise <see cref="ValidationException"/></returns>
/// <remarks>The request should implement <see cref="IValidatableRequest{TResponse}"/> and must have a registered FluentValidator validator in DI</remarks>
public sealed class ValidationBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, Result<TResponse>>
    where TRequest : IValidatableRequest
{
    private readonly IValidator<TRequest> _validator;

    public ValidationBehavior(IValidator<TRequest> validator)
    {
        _validator = validator;
    }

    public async Task<Result<TResponse>> Handle(TRequest request,
        RequestHandlerDelegate<Result<TResponse>> next,
        CancellationToken cancellationToken)
    {
        var validationResult = await _validator.ValidateAsync(request, cancellationToken);
        if (!validationResult.IsValid)
            return new Result<TResponse>(new ValidationException(validationResult.Errors));

        return await next();
    }
}