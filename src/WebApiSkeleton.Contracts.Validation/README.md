﻿# WebApiSkeleton.Contracts.Validation

`WebApiSkeleton.Contracts.Validation` contains MediatR `PipelineBehavior`
implementation to validate incoming requests that
implement [`IValidatableRequest`](../WebApiSkeleton.Contracts.Base/ContractFeatureInterfaces/IValidatableRequest.cs)
using `FluentValidation`

# Usage

Steps to add contract validation:

- Create validators that are inherited from `AbstractValidator<T>` for needed requests
- Use `AddContractValidatorsFromAssembly` (or `AddValidatorsFromAssembly` from `FluentValidation`) to add validators to
  DI
- Add validation behaviors for requests in DI
    - One way is to call `AddValidationPipelineBehaviorsFromAssembly` that will automatically search for classes that
      are inherited from `AbstractValidator` and add [`ValidationBehavior`](ValidationBehavior.cs) for requests that are
      passed as a generic parameter in DI. If any of these requests are not returning the `Result<A>` type
      the `InvalidOperationException` is thrown.
        - NOTE: in this case `ValidationBehavior` for request is automatically added, so it is highly recommended to
          mind the order
          of `IPipelineBehavior` implementations added in DI.
    - Another way is to create `ValidationBehavior` implementations by
      calling [`ValidationBehaviorCreator.GetValidationPipelineBehaviorForRequest`](ValidationBehaviorCreator.cs) method
      and then add it manually to DI by passing the `pipelineBehavior` as a service type and `validationBehavior` as
      implementation type. This way order of pipeline behaviors executed for each request is handled more clearly by the
      user, which is adding them.

## Example

Example usage shown in [Example project](../../examples/WebApiSkeleton.Contracts.Validation.Example)