using LanguageExt.Common;
using MediatR;
using WebApiSkeleton.Contracts.Base.ContractFeatureInterfaces;

namespace WebApiSkeleton.Contracts.Validation;

/// <summary>
/// Class that provides <see cref="ValidationBehavior{TRequest,TResponse}"/> implementation for the given request type
/// </summary>
public static class ValidationBehaviorCreator
{
    public static (Type pipelineBehavior, Type validationBehavior) GetValidationPipelineBehaviorForRequest(
        Type requestType)
    {
        if (requestType.GetInterface(nameof(IValidatableRequest)) is null)
            throw new InvalidOperationException(
                "Cannot generate a validation behavior type for non-validatable request type");

        var resultTypeResponseInterface = requestType.GetInterface(typeof(IResultTypeResponseRequest<>).Name);
        if (resultTypeResponseInterface is null)
            throw new InvalidOperationException(
                $"Request that implements {nameof(IValidatableRequest)} must have a Result<T> return type: {requestType.Name}");

        var responseType = resultTypeResponseInterface.GetGenericArguments().First();
        var responseResultType = typeof(Result<>).MakeGenericType(responseType);

        return (typeof(IPipelineBehavior<,>).MakeGenericType(requestType, responseResultType),
            typeof(ValidationBehavior<,>).MakeGenericType(requestType, responseType));
    }
}