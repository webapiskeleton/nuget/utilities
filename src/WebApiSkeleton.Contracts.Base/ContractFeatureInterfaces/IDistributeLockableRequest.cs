﻿namespace WebApiSkeleton.Contracts.Base.ContractFeatureInterfaces;

/// <summary>
/// Request that uses distributed locking
/// </summary>
public interface IDistributeLockableRequest
{
    
}