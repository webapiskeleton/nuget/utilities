﻿using LanguageExt.Common;

namespace WebApiSkeleton.Contracts.Base.ContractFeatureInterfaces;

/// <summary>
/// Request that returns response of type <see cref="Result{A}"/>
/// </summary>
/// <typeparam name="TResponse">Success response type that should be returned</typeparam>
public interface IResultTypeResponseRequest<TResponse> : IRequest<Result<TResponse>>;