﻿namespace WebApiSkeleton.Contracts.Base.ContractFeatureInterfaces;

/// <summary>
/// Request that is only allowed to be executed by authorized user
/// </summary>
/// <typeparam name="TResponse"><inheritdoc cref="IResultTypeResponseRequest{TResponse}"/>></typeparam>
public interface IAuthorizedRequest<TResponse> : IResultTypeResponseRequest<TResponse>, IAuthorizedRequest
{
}

/// <summary>
/// Should not be implemented on any request
/// </summary>
public interface IAuthorizedRequest
{
}
