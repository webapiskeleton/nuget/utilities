﻿namespace WebApiSkeleton.Contracts.Base.ContractFeatureInterfaces;

/// <summary>
/// Request that can be validated
/// </summary>
/// <typeparam name="TResponse"><inheritdoc cref="IResultTypeResponseRequest{TResponse}"/>></typeparam>
public interface IValidatableRequest<TResponse> : IResultTypeResponseRequest<TResponse>, IValidatableRequest
{
}

/// <summary>
/// Should not be implemented on any request
/// </summary>
public interface IValidatableRequest
{
}
