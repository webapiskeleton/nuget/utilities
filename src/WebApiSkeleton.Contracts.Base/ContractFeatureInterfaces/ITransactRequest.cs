﻿namespace WebApiSkeleton.Contracts.Base.ContractFeatureInterfaces;

/// <summary>
/// Request that uses database transaction
/// </summary>
public interface ITransactRequest
{
    
}