﻿# WebApiSkeleton.Contracts.Base

`WebApiSkeleton.Contracts.Base` contains base feature interfaces for MediatR
requests that are used across all projects.

## Usage

The project contains interfaces to generalize the behavior of some requests.

- [IResultTypeResponseRequest](./ContractFeatureInterfaces/IResultTypeResponseRequest.cs) - request that always returns `Result<TResponse>` as a response 
- [IAuthorizedRequest](./ContractFeatureInterfaces/IAuthorizedRequest.cs) - requests that can be executed only by the
  authorized user
  - is inherited from `IResultTypeResponseRequest`
- [IValidatableRequest](./ContractFeatureInterfaces/IValidatableRequest.cs) - requests that must be validated before
  handling
  - is inherited from `IResultTypeResponseRequest`
- [ITransactRequest](./ContractFeatureInterfaces/ITransactRequest.cs) - requests that use some kind of transaction (i.n.
  database transaction)
- [IDistributeLockableRequest](./ContractFeatureInterfaces/IDistributeLockableRequest.cs) - requests that are using
  distributed locking and might throw when was unable to retrieve it
