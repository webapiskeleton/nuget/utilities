﻿using System.Linq.Expressions;

namespace WebApiSkeleton.DatabaseMapping;

/// <summary>
/// Entity database column-to-property mapping
/// </summary>
/// <typeparam name="T">Entity that has to be mapped</typeparam>
public interface IDatabaseMapping<T>
{
    /// <summary>
    /// Table that entity refers to, including its schema (if present)
    /// </summary>
    public string SchemaQualifiedTableName { get; }
    
    /// <summary>
    /// Get the column name by property name
    /// </summary>
    public string? this[string propertyName] { get; }
    
    /// <summary>
    /// Get the column name by lambda expression to the property
    /// </summary>
    /// <param name="propertyExpression"></param>
    public string? this[Expression<Func<T, object>> propertyExpression] { get; }

    /// <summary>
    /// Get primary key column name
    /// </summary>
    /// <returns></returns>
    public string? GetPrimaryKey();
}