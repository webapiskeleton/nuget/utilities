﻿using System.Linq.Expressions;

namespace WebApiSkeleton.DatabaseMapping;

public abstract class BaseDatabaseMapper<T> : IDatabaseMapping<T>
{
    private string? _primaryKeyPropertyName;
    private readonly Dictionary<string, string> _propertyToDatabaseColumn = new();

    /// <inheritdoc/>
    public string SchemaQualifiedTableName { get; protected set; }

    protected BaseDatabaseMapper(string schemaQualifiedTableName)
    {
        SchemaQualifiedTableName = schemaQualifiedTableName;
    }

    /// <inheritdoc/>
    public string? this[string propertyName]
    {
        get
        {
            _propertyToDatabaseColumn.TryGetValue(propertyName, out var value);
            return value;
        }
    }

    /// <inheritdoc/>
    public string? this[Expression<Func<T, object>> propertyExpression]
    {
        get
        {
            var propertyName = ExpressionHelper.GetPropertyNameFromExpression(propertyExpression);
            return this[propertyName];
        }
    }

    /// <inheritdoc/>
    public string? GetPrimaryKey()
    {
        return _primaryKeyPropertyName is null ? null : this[_primaryKeyPropertyName];
    }

    /// <summary>
    /// Maps the property of class to the column
    /// </summary>
    /// <param name="expression">Expression to property that needs to be mapped</param>
    /// <param name="columnName">Column name to map property to</param>
    /// <param name="isPrimaryKey">Is column representing a primary key</param>
    protected void MapProperty(Expression<Func<T, object>> expression, string columnName, bool isPrimaryKey = false)
    {
        if (isPrimaryKey && _primaryKeyPropertyName is not null)
        {
            throw new InvalidOperationException(
                $"Only one primary key can be defined on entity of type {typeof(T).FullName}");
        }

        var propertyName = ExpressionHelper.GetPropertyNameFromExpression(expression);
        _propertyToDatabaseColumn.TryAdd(propertyName, columnName);
        if (isPrimaryKey) _primaryKeyPropertyName = propertyName;
    }
}