using System.Linq.Expressions;
using System.Reflection;

namespace WebApiSkeleton.DatabaseMapping;

public static class ExpressionHelper
{
    /// <summary>
    /// Gets the property name from property expression
    /// </summary>
    /// <param name="expression">Property expression</param>
    /// <returns>The name of property</returns>
    /// <exception cref="InvalidOperationException">Provided class does not have a given property or expression references not to property</exception>
    public static string GetPropertyNameFromExpression<T>(Expression<Func<T, object>> expression)
    {
        var lambda = (LambdaExpression)expression;
        var memberExpression = lambda.Body switch
        {
            UnaryExpression unary => (MemberExpression)unary.Operand,
            MemberExpression member => member,
            _ => throw new InvalidOperationException("Cannot get property name from given expression")
        };

        if (memberExpression.Member is not PropertyInfo propertyInfo)
        {
            throw new InvalidOperationException("Expression does not refer to a property");
        }

        return propertyInfo.Name;
    }
}