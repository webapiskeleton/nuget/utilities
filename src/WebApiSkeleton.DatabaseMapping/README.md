﻿# WebApiSkeleton.DatabaseMapping

`WebApiSkeleton.DatabaseMapping` contains features to implement property-to-column database mapping using Expression.

## Usage

Models that represent a database table and need to be mapped should inherit
from [BaseDatabaseMapper](./BaseDatabaseMapper.cs). Mapping is done by using `MapProperty` method: pass a
lambda-expression to the property and the column name. If non-property member is passed, the method will throw.

## Example

Example usage shown in [Example project](../../examples/WebApiSkeleton.DatabaseMapping.Example)