﻿using StackExchange.Redis;

namespace WebApiSkeleton.DistributeLockUtilities.Settings;

public sealed class RedisSettings
{
    public static string RedisDistributedLockServiceKey => "redis-lock-service";

    public required IConnectionMultiplexer ConnectionMultiplexer { get; init; }
    public required int DatabaseNumber { get; init; }
}