﻿namespace WebApiSkeleton.DistributeLockUtilities.Settings;

public class SemaphoreSettings
{
    public required string SemaphoreName { get; set; } = null!;
    public required int MaximumRequestCount { get; set; }
}