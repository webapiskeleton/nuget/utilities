﻿namespace WebApiSkeleton.DistributeLockUtilities.Exceptions;

public class DistributedLockException : Exception
{
    public DistributedLockException() : base("This request is being locked by another")
    {
        
    }
    public DistributedLockException(string? message) : base(message ?? "This request is being locked by another")
    {
    }
}