﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using WebApiSkeleton.DistributeLockUtilities.Services;
using WebApiSkeleton.DistributeLockUtilities.Services.Redis;
using WebApiSkeleton.DistributeLockUtilities.Settings;

namespace WebApiSkeleton.DistributeLockUtilities;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection TryAddRedisDistributedLockService(this IServiceCollection services)
    {
        services.TryAddSingleton<IDistributedLockService, RedisDistributedLockService>();
        return services;
    }
}