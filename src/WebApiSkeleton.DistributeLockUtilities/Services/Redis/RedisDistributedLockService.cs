﻿using System.Diagnostics;
using Medallion.Threading;
using Medallion.Threading.Redis;
using StackExchange.Redis;
using WebApiSkeleton.DistributeLockUtilities.Exceptions;
using WebApiSkeleton.DistributeLockUtilities.Settings;

namespace WebApiSkeleton.DistributeLockUtilities.Services.Redis;

public class RedisDistributedLockService : IDistributedLockService
{
    private readonly IDatabase _database;

    public RedisDistributedLockService(RedisSettings settings)
    {
        _database = settings.ConnectionMultiplexer.GetDatabase(settings.DatabaseNumber);
    }

    /// <inheritdoc/>
    public async Task<IDistributedSynchronizationHandle?> AcquireLockAsync(string lockName)
    {
        var distributedLock = new RedisDistributedLock(lockName, _database);
        return await distributedLock.TryAcquireAsync();
    }

    /// <inheritdoc/>
    public async Task<IDistributedSynchronizationHandle?> AcquireSemaphoreAsync(SemaphoreSettings settings)
    {
        var semaphore = new RedisDistributedSemaphore(settings.SemaphoreName, settings.MaximumRequestCount, _database);
        return await semaphore.TryAcquireAsync(TimeSpan.FromSeconds(1));
    }

    /// <inheritdoc/>
    public async Task WithDistributedLockAsync(string key, Func<Task> action, bool throwOnFailure = true)
    {
        var stopWatch = new Stopwatch();
        stopWatch.Start();
        while (true)
        {
            if (stopWatch.Elapsed >= TimeSpan.FromMinutes(1))
            {
                throw new DistributedLockException("Could not take a lock for 1 minute, aborting");
            }

            await using var lockHandle = await AcquireLockAsync(key);

            if (lockHandle is null)
            {
                if (throwOnFailure)
                    throw new DistributedLockException($"Lock key {key} is already taken");

                await Task.Delay(50);
                continue;
            }

            await action();
            return;
        }
    }
}