﻿using Medallion.Threading;
using WebApiSkeleton.DistributeLockUtilities.Exceptions;
using WebApiSkeleton.DistributeLockUtilities.Settings;

namespace WebApiSkeleton.DistributeLockUtilities.Services;

public interface IDistributedLockService
{
    /// <summary>
    /// Acquire a distributed lock that is released on dispose 
    /// </summary>
    /// <param name="lockName">The name of acquiring lock</param>
    /// <returns>IDisposable lock object</returns>
    public Task<IDistributedSynchronizationHandle?> AcquireLockAsync(string lockName);

    /// <summary>
    /// Acquire a distributed semaphore that is released on dispose 
    /// </summary>
    /// <param name="settings">Semaphore settings</param>
    /// <returns>IDisposable semaphore lock object</returns>
    public Task<IDistributedSynchronizationHandle?> AcquireSemaphoreAsync(SemaphoreSettings settings);

    /// <summary>
    /// Execute given action with a distributed lock
    /// </summary>
    /// <param name="key">The name of acquiring lock</param>
    /// <param name="action">Action to execute</param>
    /// <param name="throwOnFailure">Throw if lock could not be taken or try again after some delay</param>
    /// <exception cref="DistributedLockException">If <see cref="throwOnFailure"/> is set to true and lock could not be taken, or if lock is not being taking for too long</exception>
    public Task WithDistributedLockAsync(string key, Func<Task> action, bool throwOnFailure = true);
}