﻿# WebApiSkeleton.DistributeLockUtilities

`WebApiSkeleton.DistributeLockUtilities` contains dependencies to implement different types of distributed locks.

## Usage

### Settings

[RedisSettings](./Settings/RedisSettings.cs) provides properties to set up Redis `IConnectionMultiplexer` and the
database number that is used. If multiple redis instances are required it is better to use .NET 8 Keyed services
feature.

[SemaphoreSettings](./Settings/SemaphoreSettings.cs) are used
in [IDistributedLockService](./Services/IDistributedLockService.cs) to resolve semaphores.

### Description

[IDistributedLockService](./Services/IDistributedLockService.cs) is an abstraction to the lock service. It provides base
methods to acquire locks and semaphores.
Default implementation is [RedisDistributedLockService](./Services/Redis/RedisDistributedLockService.cs).

In default redis implementation [DistributedLockException](./Exceptions/DistributedLockException.cs) is
thrown `WithDistributedLockAsync` method has `throwOnFailure` argument set to `true` or lock couldn't be taken for more
than 1 minute.