﻿# WebApiSkeleton.Utilities

Contains packages that are used across all `WebApiSkeleton` projects, including NuGet packages and template itself.

## Description

- [`WebApiSkeleton.Contracts.Base`](./src/WebApiSkeleton.Contracts.Base) contains base feature interfaces for MediatR
  requests that are used across all projects;
- [`WebApiSkeleton.Contracts.Validation`](./src/WebApiSkeleton.Contracts.Validation) contains MediatR `PipelineBehavior`
  implementation to validate incoming requests using `FluentValidation`;
- [`WebApiSkeleton.DistributeLockUtilities`](./src/WebApiSkeleton.DistributeLockUtilities) contains dependencies to
  implement different types of distributed locks;
- [`WebApiSkeleton.DatabaseMapping`](./src/WebApiSkeleton.DatabaseMapping) contains features to implement
  property-to-column database mapping using `Expression`.

## Versioning

All projects are versioned using following format: `major.minor.patch`. Versioning rules for all projects:

- `patch` needs to be incremented when any minor change is made to the project, such as bugfixes or small
  project-specific features added
- `minor` needs to be incremented when new template-wide feature is implemented. In this case all of the projects must
  have the same version set
- `major` needs to be incremented when the `WebApiSkeleton` template has experienced significant changes, that need to
  upgrade all of the template packages. In this case all of the projects must
  have the same version set